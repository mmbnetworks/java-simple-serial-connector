package jssc;

import org.junit.jupiter.api.Test;

public class SerialPortTest {

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @Test
    public void test() {
        String osName = System.getProperty("os.name");
        String architecture = System.getProperty("os.arch");
        String userHome = System.getProperty("user.home");
        String fileSeparator = System.getProperty("file.separator");
        String tmpFolder = System.getProperty("java.io.tmpdir");

        System.out.println("osName: " + osName);
        System.out.println("osArch: " + architecture);
        System.out.println("user.home: " + userHome);
        System.out.println("file separator: " + fileSeparator);
        System.out.println("tmpFolder: " + tmpFolder);
    }

}